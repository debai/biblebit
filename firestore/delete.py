from google.cloud import firestore

# Project ID is determined by the GCLOUD_PROJECT environment variable
db = firestore.Client()
test_ref = db.collection(u'1min')

query = test_ref.stream()

count = 0
for i in query:
    #print(i.id)
    db.collection(u'1min_test').document(i.id).delete()
    if count % 1000 == 0:

