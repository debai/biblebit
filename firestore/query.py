from google.cloud import firestore

# Project ID is determined by the GCLOUD_PROJECT environment variable
db = firestore.Client()



"""
test_ref = db.collection(u'bible')

#query = test_ref.where(u'stock', u'==', u'MSFT').stream()
query = test_ref.where(u'chapter', u'==', 1).stream()



for i in query:
    print(u'{} => {}'.format(i.id, i.to_dict()))



user = {'num_correct': 0, 'phone_number': '16472082778', 'msg_count': 0, 'last_name': 'bai', 'active': True,
        'gambit_id': '2peter.1.1.1.1', 'id': 0, 'first_name': 'david', 'last_reminder': '2019.11.30', 'reminder': '17:00:00',
                   'current': {'book': '2peter', 'part': 0, 'chapter': 1, 'last_answer': ['hello'], 'verse': 1}}
"""

game_table_query = db.collection(u'games').where(u'gambit_id', u'==', "matt.5.1.1.1").stream()

for i in game_table_query:
    print(u'{} => {}'.format(i.id, i.to_dict()))

