from google.cloud import firestore
from datetime import datetime
from datetime import timedelta
import random

# Project ID is determined by the GCLOUD_PROJECT environment variable
db = firestore.Client()
query_ref = db.collection(u'users')


hhmmss = datetime.now().strftime("%H:%M:%S")
yyyymmdd = datetime.strftime(datetime.now() - timedelta(1), '%Y.%m.%d')
query = query_ref.where(u'reminder', u'<=', hhmmss).where(u'last_sent', u'==', yyyymmdd).stream()

user = None
user_doc_id = None
for i in query:
    print(u'{} => {}'.format(i.id, i.to_dict()))
    user_doc_id = i.id
    user = i.to_dict()




