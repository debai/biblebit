import google.cloud.firestore as firestore
import random
from flask import Flask, request
from datetime import datetime
import os
from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse


app = Flask(__name__)

DB = firestore.Client()
SPECIAL = ["-", ",", ".", "''", "'", ";", "(", ")", "?", ":"]


def remove_front_spaces(word):
    """
    :param word:
    :return:
    """

    if len(word) > 0 and word[0] == " ":
        return remove_front_spaces(str(word[1:]))
    elif word == " ":
        return ""
    else:
        return word


def remove_back_spaces(word):
    """
    :param word:
    :return:
    """

    if len(word) > 0 and word[-1] == " ":
        return remove_back_spaces(str(word[:-1]))
    elif word == " ":
        return ""
    else:
        return word


def alpha_standard(word):
    """
    :param word: a string
    :return: string with all lower case letters and non alphabet characters removed. Also spaces at the front and back are removed
    """
    global SPECIAL
    new_word = ""

    for letter in word:
        if letter not in SPECIAL:
            new_word += letter

    new_word = new_word.lower()
    new_word = remove_front_spaces(new_word)
    new_word = remove_back_spaces(new_word)

    return new_word


def checker(user, user_resp, msg):

    user_resp = alpha_standard(user_resp)

    if user_resp == user['last_answer']:
        user['num_correct'] += 1
        msg += "Great job!\n"

        # Update user's gambit if necessary
        if user['num_correct'] == 3:
            game_table_query = DB.collection(u'game_table')\
                .where(u'gambit_id', u'==', user["gambit_id"])\
                .limit(1)\
                .stream()

            gambit_info = next(game_table_query).to_dict()
            user['gambit_id'] = gambit_info['next_gambit_id']
            user['num_correct'] = 0

            former_biblebit = gambit_info['gambit_id'].split(".")[-2]
            current_biblebit = gambit_info['next_gambit_id'].split(".")[-2]

            if former_biblebit != current_biblebit:
                gambit_id = gambit_info['next_gambit_id'].split(".")
                msg += gambit_id[0] + ":" + gambit_id[1] + ":" + gambit_id[2] + " Part " + gambit_id[3] + "\n"

    elif user_resp != "":
        user['num_correct'] = 0
        msg += "The correct answer is: " + user['last_answer'] + "\n"

    return msg


def next_msg(user, msg):
    if user["msg_count"] >= 5:
        msg += "That's it for today."

    else:
        gambit_query = DB.collection(u'games').where(u'gambit_id', u'==', user["gambit_id"]).stream()
        gambit_list = list(map(lambda x: x.to_dict(), gambit_query))
        gambit = gambit_list[random.randrange(0, len(gambit_list))]

        user['msg_count'] += 1
        user['last_answer'] = gambit['answer']

        msg += "\n" + gambit['question'] + "\n"

    return msg


def reminders(phone_number):

    # Get user info from firestore
    user_query = next(DB.collection(u'users').where(u'phone_number', u'==', phone_number).stream())
    user_db_id = user_query.id
    user = user_query.to_dict()
    user["msg_count"] = 0

    # new outgoing message
    gambit_id = user["gambit_id"].split(".")
    msg = gambit_id[0] + ":" + gambit_id[1] + ":" + gambit_id[2] + " Part " + gambit_id[3] + "\n"
    msg += next_msg(user, "")

    # Recording the outgoing msg in firestore
    DB.collection('msgs').add({"user_id": user["uid"], "msg": msg, "time": datetime.now(), "sys": True})

    # update user in firestore
    DB.collection(u'users').document(user_db_id).set(user)

    # help website msg
    msg += "\n-------\nfor help, visit: https://gitgud.github.io/markdown/"

    # Sending the sms
    client = Client(os.environ["TWILIO_ACCOUNT_SID"], os.environ["TWILIO_AUTH_TOKEN"])
    client.messages.create(body=msg, from_='+12262127771', to=user["phone_number"])

    return None


@app.route('/twilio', methods=['GET', 'POST'])
def user_text():
    phone_number = request.values.get('From', None)
    user_response = request.values.get('Body', None)
    msg = ""

    # Get user info from firestore
    user_query = next(DB.collection(u'users').where(u'phone_number', u'==', phone_number).stream())
    user_db_id = user_query.id
    user = user_query.to_dict()

    # record user response
    DB.collection('msgs').add({"msg": request.values, "time": datetime.now(), "uid": user["uid"]})

    # creating new message
    msg = checker(user, user_response, msg)
    msg = next_msg(user, msg)

    # update user in firestore
    DB.collection(u'users').document(user_db_id).set(user)

    # Recording the outgoing msg in firestore
    DB.collection('msgs').add({"user_id": user["uid"], "msg": msg, "time": datetime.now()})

    # creating the response
    resp = MessagingResponse()
    resp.message(msg)

    return str(resp)


@app.route('/david_reminder', methods=['GET', 'POST'])
def david_reminder():
    reminders("+16472082778")
    return "yes"


@app.route('/paul_reminder', methods=['GET', 'POST'])
def paul_reminder():
    reminders("+16478959212")
    return "yes"


@app.route('/time', methods=['GET', 'POST'])
def time():
    return str(datetime.now())


if __name__ == '__main__':
    app.run(debug=True)


"""
    client = Client(os.environ["account_sid"], os.environ["auth_token"])
    client.messages.create(body=msg, from_='+12262127771', to=user["phone_number"])
    """






