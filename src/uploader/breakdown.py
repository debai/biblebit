"""
breaks down text into games

"""

import random
import collections
import pandas as pd

MAX_QUESTION_PERMUTATIONS = 100
CSV_OUTPUT_FILE = "games.csv"


def make_combos(num_items, items):
    """
    :param num_items: int, number of items in a combo
    :param items: list of items to make combos out of
    :return: list of lists. Inner lists reps combos, inner lists items are sorted within the inner list
    """

    all_combos = []

    if num_items > len(items):
        return [[item] for item in items]

    if num_items == 1:
        return [[items[i]] for i in range(len(items))]

    for i in range(num_items - 1, len(items)):
        temp = make_combos(num_items - 1, items[0:i])

        for temp_item in temp:
            all_combos += [[items[i]] + temp_item]

    all_combos = list(map(sorted, all_combos))

    return all_combos


def make_word_letter_combos(bit_word_list, num_blank_words, num_blank_letters):
    """
    :param bit_word_list: list of str, each word in the bit is an item in the list
    :param num_blank_words: int, number of words that should be questions
    :param num_blank_letters: int, number of letters in the word that should be questioned
    :return: list of orderdict, each Od is has key:int, index of word inside bit_word_list and val:list of int, index of
    letters to question
    """

    # creating list of word combos
    word_combos = make_combos(num_blank_words, [i for i in range(len(bit_word_list))])
    random.shuffle(word_combos)

    if len(word_combos) > MAX_QUESTION_PERMUTATIONS:
        word_combos = word_combos[:MAX_QUESTION_PERMUTATIONS]

    # creating a dictionary of possible letter combos key=length of word, val= list of letter combos
    all_letter_combos = {i: [j for j in range(i)] for i in range(1, 19)}
    for i in all_letter_combos:
        all_letter_combos[i] = make_combos(num_blank_letters, all_letter_combos[i])
        random.shuffle(all_letter_combos[i])

    # combining word combos and letter combos
    word_letter_combos = []
    for word_combo in word_combos:
        temp_combo = collections.OrderedDict()

        for word_pos in word_combo:
            temp_combo[word_pos] = []
            letter_combos = all_letter_combos[len(bit_word_list[word_pos])]
            letter_combo = letter_combos[random.randrange(len(letter_combos))]

            for letter_pos in letter_combo:
                temp_combo[word_pos] += [letter_pos]

        word_letter_combos += [temp_combo]

    return word_letter_combos


def question1(bit, marker, num_blank_words):
    """

    :param bit: str, the biblebit
    :param marker: str that has [] in place of the words which will be questioned
    :param num_blank_words: the number of [] to blank out in one question
    :return: tuple(listA, listB). listA is list of str, reps questions. listB is list of str, reps answers.
    """

    questions = []
    answers = []
    marker = marker.split(" ")
    bit_word_list = bit.split(" ")

    positions = [i for i in range(len(marker)) if "[?]" in marker[i]]
    word_combos = make_combos(num_blank_words, positions)

    if len(word_combos) > MAX_QUESTION_PERMUTATIONS:
        word_combos = word_combos[:MAX_QUESTION_PERMUTATIONS]

    random.shuffle(word_combos)  # shuffling for better question generation

    # creating the questions
    for word_combo in word_combos:

        question = bit_word_list[:]
        answer = ""
        for pos in word_combo:

            question[pos] = marker[pos]
            answer += " " + bit_word_list[pos]

        answer = answer[1:]

        question = " ".join(question)
        questions += [question]
        answers += [answer]

    return questions, answers


def question2(bit, num_blank_words, num_blank_letters):
    """
    This question chooses random words in the bit and replaces them with [int, int...] < the ints specify the pos of
    letters that should be typed back in the answer. The other words in the bit which aren't questions, remain intact

    :param bit: str, the biblebit
    :param num_blank_words: the number of [] to blank out in one question
    :param num_blank_letters: int, number of letters in the word that should be questioned
    :return: tuple(listA, listB). listA is list of str, reps questions. listB is list of str, reps answers.
    """

    questions = []
    answers = []
    bit_word_list = bit.split(" ")
    word_letter_combos = make_word_letter_combos(bit_word_list, num_blank_words, num_blank_letters)

    # making the word combos into questions
    for word_combo in word_letter_combos:
        question = ""
        answer = ""

        for word_pos in range(len(bit_word_list)):

            if word_pos in word_combo:
                question += "["

                for letter_pos in word_combo[word_pos]:
                    question += str(letter_pos + 1) + ", "  # plus 1 is there cause humans count letter positions from 1
                    answer += bit_word_list[word_pos][letter_pos]

                question = question[:len(question) - 2]  # getting rid of last comma and space
                question += "] "
                answer += " "

            else:
                question += bit_word_list[word_pos] + " "

        question = question[:len(question) - 1]  # getting rid of last space
        answer = answer[:len(answer) - 1]  # getting rid of last space
        answer = answer.lower()

        questions += [question]
        answers += [answer]

    return questions, answers


def question3(bit_word_list, num_blank_words, num_blank_letters):
    """
    This question chooses random words in the bit and replaces them with intW[intL, intL...] < the intW specifies the
    pos the word that is missing. The intL's specify letters that should be typed back in the answer.
    The other words in the bit which aren't questions, are taken away

    :param bit: str, the biblebit
    :param num_blank_words: the number of [] to blank out in one question
    :param num_blank_letters: int, number of letters in the word that should be questioned
    :return: tuple(listA, listB). listA is list of str, reps questions. listB is list of str, reps answers.
    """

    questions = []
    answers = []
    bit_word_list = bit_word_list.split(" ")
    word_letter_combos = make_word_letter_combos(bit_word_list, num_blank_words, num_blank_letters)

    for word_combo in word_letter_combos:
        question = ""
        answer = ""

        for word_pos in range(len(bit_word_list)):
            if word_pos in word_combo:

                question += str(word_pos + 1) + "["   # the plus 1 is there cause humans count letter positions from 1

                for letter_pos in word_combo[word_pos]:
                    question += str(letter_pos + 1) + ", "
                    answer += bit_word_list[word_pos][letter_pos]

                question = question[:len(question) - 2]  # getting rid of last comma and space
                question += "]  "

                answer += " "

        question = question[:len(question) - 2]  # getting rid of last 2 spaces
        answer = answer[:len(answer) - 1]  # getting rid of last space
        answer = answer.lower()

        questions += [question]
        answers += [answer]

    return questions, answers


bit = "Seeing the crowds he went up on the mountain and when he sat down his disciples came to him"
marker = "[?] the [?] [?] went up on the [?] and when he sat down his [?] came to [?]"


q1 = question1(bit, marker, 3)
q2 = question2(bit, 3, 2)
q3 = question3(bit, 3, 2)


def make_question_csv(qa_list, gambit_id_list):
    rows  = []

    while len(qa_list):
        current_qa = qa_list.pop(0)
        current_questions = current_qa[0]
        current_answers = current_qa[1]
        current_gambit_id = gambit_id_list.pop(0)

        for i in range(len(current_questions)):
            rows += [{"question": current_questions[i], "answer": current_answers[i], "gambit_id":current_gambit_id}]

    df = pd.DataFrame(rows)
    df.to_csv(CSV_OUTPUT_FILE, index=False)


make_question_csv([q1, q2, q3], ["matt.5.1.1.1", "matt.5.1.1.2", "matt.5.1.1.3"])


