"""
writes games to firestore
"""

import google.cloud.firestore as firestore
import pandas as pd

db = firestore.Client()
df = pd.read_csv("games.csv")
rows = df.to_dict(orient="records")

for row in rows:
    db.collection('games').add(row)






